package ru.apolyakov.tm;

import ru.apolyakov.tm.api.enumerated.Status;
import ru.apolyakov.tm.bootstrap.Bootstrap;
import ru.apolyakov.tm.model.Task;

public class App {

    public static void main(String[] args) {
       final Bootstrap bootstrap = new Bootstrap();
       bootstrap.run(args);
    }

}
