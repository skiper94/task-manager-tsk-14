package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // TASK CONTROLLER
    List<Task> findAllTaskByProjectId(String projectId);

    // TASK CONTROLLER
    Task bindTaskByProjectId(String projectId, String taskId);

    // TASK CONTROLLER
    Task unbindTaskFromProject(String projectId);

    // PROJECT CONTROLLER
    List<Task> removeTasksByProjectId(String projectId);

    Project removeProjectById(String projectId);
}
