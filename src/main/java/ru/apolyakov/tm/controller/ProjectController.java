package ru.apolyakov.tm.controller;

import ru.apolyakov.tm.api.enumerated.Sort;
import ru.apolyakov.tm.api.enumerated.Status;
import ru.apolyakov.tm.api.service.IProjectService;
import ru.apolyakov.tm.api.controller.IProjectController;
import ru.apolyakov.tm.api.service.IProjectTaskService;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private  final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService){
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjects(){
        System.out.println("[PROJECTS LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects = new ArrayList<>();
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDispayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Project project: projects){
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createProject(){
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects(){
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex(){
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null){
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById(){
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null){
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProject(final Project project){
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDispayName());
    }

    @Override
    public void showProjectByName(){
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(name);
        if (project == null){
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex(){
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.removeProjectByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectById(){
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName(){
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIdWithChildren(){
        System.out.println("[DELETE PROJECT]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex(){
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(index, name, description);
        if (projectUpdated == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById(){
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(id, name, description);
        if (projectUpdated == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.startProjectByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startProjectByName() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishProjectById() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishProjectByIndex() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.finishProjectByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishProjectByName() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishProjectByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusById(){
        System.out.println("[SETTING STATUS TO PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        System.out.println("to find the project id use the command: project-list");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Project project;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: project = projectService.changeProjectStatusById(projectId, Status.NOT_STARTED);break;
            case 2: project = projectService.changeProjectStatusById(projectId, Status.IN_PROGRESS);break;
            case 3: project = projectService.changeProjectStatusById(projectId, Status.COMPLETE);break;
            default:
                project = null;
        }
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }


    @Override
    public void changeProjectStatusByIndex(){
        System.out.println("[SETTING STATUS TO PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        System.out.println("to find the project index use the command: project-list");
        final Integer projectIndex = TerminalUtil.nextNumber() -1;
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Project project;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: project = projectService.changeProjectStatusByIndex(projectIndex, Status.NOT_STARTED);break;
            case 2: project = projectService.changeProjectStatusByIndex(projectIndex, Status.IN_PROGRESS);break;
            case 3: project = projectService.changeProjectStatusByIndex(projectIndex, Status.COMPLETE);break;
            default:
                project = null;
        }
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }


    @Override
    public void changeProjectStatusByName(){
        System.out.println("[SETTING STATUS TO PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        System.out.println("to find the project name use the command: project-list");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Project project;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: project = projectService.changeProjectStatusByName(taskName, Status.NOT_STARTED);break;
            case 2: project = projectService.changeProjectStatusByName(taskName, Status.IN_PROGRESS);break;
            case 3: project = projectService.changeProjectStatusByName(taskName, Status.COMPLETE);break;
            default:
                project = null;
        }
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
