package ru.apolyakov.tm.controller;

import ru.apolyakov.tm.api.controller.ITaskController;
import ru.apolyakov.tm.api.enumerated.Sort;
import ru.apolyakov.tm.api.enumerated.Status;
import ru.apolyakov.tm.api.service.IProjectTaskService;
import ru.apolyakov.tm.api.service.ITaskService;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.service.ProjectTaskService;
import ru.apolyakov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private  final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(final ITaskService taskService, final IProjectTaskService projectTaskService){
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }


    @Override
    public void showTasks(){
        System.out.println("[TASKS LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasksSort = new ArrayList<>();
        if (sort == null || sort.isEmpty()) tasksSort = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDispayName());
            tasksSort = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task: tasksSort){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void createTask(){
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks(){
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex(){
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskById(){
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    private void showTask(final Task task){
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDispayName());
    }

    @Override
    public void showTaskByName(){
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex(){
        System.out.println("[DELETE TASK]");
        System.out.println("ENTER INDEX");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeTaskByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskById(){
        System.out.println("[DELETE TASK]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeTaskByName(){
        System.out.println("[DELETE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex(){
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById(){
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null){
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startTaskByName() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskById() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskByIndex() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.finishTaskByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void finishTaskByName() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishTaskByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

     @Override
     public void findAllTaskByProjectId(){
         System.out.println("[SEARCH TASKS BY PROJECT ID]");
         System.out.println("to find the project id use the command: project-list");
         System.out.println("ENTER PROJECT ID:");
         final String projectId = TerminalUtil.nextLine();
         final List<Task> tasks = projectTaskService.findAllTaskByProjectId(projectId);
         if (tasks.isEmpty() || tasks == null) System.out.println("[FAIL]");
         else System.out.println(tasks.toString());
     }

     @Override
     public void bindTaskByProjectId(){
         System.out.println("[BINDING TASK BY PROJECT ID]");
         System.out.println("to find the project id use the command: project-list");
         System.out.println("ENTER PROJECT ID:");
         final String projectId = TerminalUtil.nextLine();
         System.out.println("ENTER TASK ID:");
         System.out.println("to find the task id use the command: task-list");
         final String taskId = TerminalUtil.nextLine();
         final Task task = projectTaskService.bindTaskByProjectId(projectId, taskId);
         if (task == null) System.out.println("[FAIL]");
         else System.out.println("[TASK ADDED TO PROJECT SUCCESSFUL]");
     }

     @Override
     public void unbindTaskByProjectId(){
         System.out.println("[DELETING TASK FROM PROJECT BY ID]");
         System.out.println("ENTER TASK ID:");
         System.out.println("to find the task id use the command: task-list");
         final String taskId = TerminalUtil.nextLine();
         final Task task = projectTaskService.unbindTaskFromProject(taskId);
         if (task == null) System.out.println("[FAIL]");
         else System.out.println("[TASK DELETED FROM PROJECT SUCCESSFUL]");
     }

     @Override
     public void removeAllTaskByProjectId(){
         System.out.println("[REMOVING ALL TASKS BY PROJECT ID]");
         System.out.println("ENTER PROJECT ID:");
         System.out.println("to find the project id use the command: project-list");
         final String projectId = TerminalUtil.nextLine();
         final List<Task> tasks = projectTaskService.removeTasksByProjectId(projectId);
         if (tasks.isEmpty()) System.out.println("PROJECT IS CLEARED");
         else System.out.println("[FAIL]");
     }

     @Override
     public void changeTaskStatusById(){
         System.out.println("[SETTING STATUS TO TASK BY ID]");
         System.out.println("ENTER TASK ID:");
         System.out.println("to find the task id use the command: task-list");
         final String taskId = TerminalUtil.nextLine();
         System.out.println("ENTER NUMBER OF STATUS:");
         System.out.println("1: [NOT STARTED]");
         System.out.println("2: [IN PROGRESS]");
         System.out.println("3: [COMPLETE]");
         final Task task;
         final Integer statusNum = TerminalUtil.nextNumber();
         switch (statusNum) {
             case 1: task = taskService.changeTaskStatusById(taskId, Status.NOT_STARTED);break;
             case 2: task = taskService.changeTaskStatusById(taskId, Status.IN_PROGRESS);break;
             case 3: task = taskService.changeTaskStatusById(taskId, Status.COMPLETE);break;
             default:
                 task = null;
         }
         if (task == null) System.out.println("[FAIL]");
         else System.out.println("[OK]");
     }

    @Override
    public void changeTaskStatusByIndex(){
        System.out.println("[SETTING STATUS TO TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        System.out.println("to find the task index use the command: task-list");
        final Integer taskIndex = TerminalUtil.nextNumber() -1;
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Task task;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: task = taskService.changeTaskStatusByIndex(taskIndex, Status.NOT_STARTED);break;
            case 2: task = taskService.changeTaskStatusByIndex(taskIndex, Status.IN_PROGRESS);break;
            case 3: task = taskService.changeTaskStatusByIndex(taskIndex, Status.COMPLETE);break;
            default:
                task = null;
        }
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByName(){
        System.out.println("[SETTING STATUS TO TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        System.out.println("to find the task name use the command: task-list");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Task task;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: task = taskService.changeTaskStatusByName(taskName, Status.NOT_STARTED);break;
            case 2: task = taskService.changeTaskStatusByName(taskName, Status.IN_PROGRESS);break;
            case 3: task = taskService.changeTaskStatusByName(taskName, Status.COMPLETE);break;
            default:
                task = null;
        }
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}
